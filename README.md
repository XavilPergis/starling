# Starling DAW

Starling is a free-of-cost and open-source Digital Audio Workstation, or DAW for short.
It is in a very early stage of development, and is not yet usable for music production. (Bear with us, we're working on it.)

The codebase is very young, and is rapidly growing and changing, so communication between contributors is especially important.
For this reason, if you would like to contribute to the project, please contact Frank at ottercode@yahoo.com

## Building

Starling uses CMAKE, and should work with any compiler that supports the C++17 standard.
Currently, the only required external dependency is the Qt5 framework, although this will likely change in the future.

## License

The program as a whole is licensed under the GNU General Public License Version 3 (GPLv3), which can be found in the COPYING file.
Some individual files may be licensed under more permissive licenses, such as LGPL or MIT, which are "one-way compatible" with GPLv3;
that is, these files can be included in a project licensed under GPLv3, but files licensed under GPLv3 cannot be included in a
project licensed under one of those licenses.
