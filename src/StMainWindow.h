/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HELLO_WINDOW_H
#define HELLO_WINDOW_H

#include <QMainWindow>

class ClipWidget;
class LaneWidget;
class AudioEngine;
class SineModule;
class ClipLane;

namespace Ui
{
    class StMainWindow;
}

/**
    StMainWindow is the main window of the Starling DAW. It provides a
    layout for all other interfaces, and connects them together using
    signals and slots.
 */
class StMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StMainWindow(QWidget* parent = 0);
    ~StMainWindow();

public slots:
    void addMIDITrack();
    void openPianoRoll(ClipWidget* clipWidget);
    void openSignalChainEditor(LaneWidget* laneWidget);
    void playButtonPressed();
    void stopButtonPressed();
    void objectDestroyed(QObject* object = nullptr);

private:
    Ui::StMainWindow *ui;
    LaneWidget* mLaneWidget;
    AudioEngine* mAudioEngine;
    QWidget* mCurrentLowerEditor = nullptr;
    std::shared_ptr<SineModule> mSineModule;
};

#endif // HELLO_WINDOW_H