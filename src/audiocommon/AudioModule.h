/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUDIO_MODULE_H
#define AUDIO_MODULE_H

#include "AudioCommon.h"
#include "MIDI.h"
#include <QObject>

class QWidget;

class AudioModule : public QObject
{
    Q_OBJECT

public:
    virtual ~AudioModule(){}
    
    virtual void generateSamples(StBuffers& inBuffers, StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI) = 0;
    virtual void reset() = 0;
    virtual QWidget* makeGUI(QWidget* parent) = 0;
};

#endif // AUDIO_MODULE_H