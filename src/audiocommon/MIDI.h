/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ST_MIDI_H
#define ST_MIDI_H

#include <cstdint>
#include <cstddef>
#include <vector>

struct MIDIWord
{
    uint8_t bytes[4];
};

struct MIDISequence
{
    MIDIWord* words;
    size_t numWords;
    uint64_t* offsets;
    size_t numOffsets;
};

/**
    MIDIParser is a helper class for parsing MIDI Sequences. It is intended
    to be instantiated on the stack in any function that needs to parse a
    MIDI sequence. It does not take ownership of any resources passed to
    its constructor, and holds only pointers to those resources.
 */
class MIDIParser
{
public:
    MIDIParser(const MIDIWord* words, size_t numWords, const uint64_t* offsets, size_t numOffsets);
    MIDIParser(const MIDISequence& sequence);
    uint8_t mt();
    size_t packetSize();
    bool isNoteOn();
    bool isNoteOff();
    uint16_t velocity();
    uint8_t note();
    bool next();
    bool next(size_t offset);

private:
    const MIDIWord* mWords;
    size_t mWordsLeft;
    const uint64_t* mOffsets;
    size_t mOffsetsLeft;
    bool isFirstWord = true;
};

#endif // ST_MIDI_H