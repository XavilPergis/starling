/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AudioEngine.h"
#include <cstdint>
#include <memory>
#include <mutex>
#include <vector>
#include <cmath>
#include <soundio.h>
#include "SampleConversion.h"
#include "AudioModuleChain.h"

writeSamplesPFN getSampleConvertFunc(SoundIoFormat format)
{
    switch(format)
    {
    case SoundIoFormatFloat64NE:
        return writeSamples_float64ne;
    case SoundIoFormatFloat32NE:
        return writeSamples_float32ne;
    case SoundIoFormatS32NE:
        return writeSamples_s32ne;
    case SoundIoFormatS16NE:
        return writeSamples_s16ne;
    default:
        return nullptr;
    }
}

AudioEngine::AudioEngine()
{
    
}

AudioEngine::~AudioEngine()
{
    stopSoundIo();

    std::lock_guard<std::mutex> lock(mChainMutex);
}

double AudioEngine::getSampleRate()
{
    return mSampleRate;
}

AudioEngine::IOMode AudioEngine::getIOMode()
{
    return mIOMode;
}

void AudioEngine::setSampleRate(double sampleRate)
{
    mSampleRate = sampleRate;
}

void AudioEngine::initSoundIO()
{
    int err;

    mSoundIo = soundio_create();
    if(!mSoundIo)
    {
        fprintf(stderr, "Could not create SoundIo!!\n");
        return;
    }

    if((err = soundio_connect(mSoundIo)))
    {
        fprintf(stderr, "Could not connect SoundIo: %s!!\n", soundio_strerror(err));
        return;
    }

    soundio_flush_events(mSoundIo);

    int defaultOutDeviceIndex = soundio_default_output_device_index(mSoundIo);
    if(defaultOutDeviceIndex < 0)
    {
        fprintf(stderr, "No output device found!!\n");
        return;
    }

    mSoundIoOutDevice = soundio_get_output_device(mSoundIo, defaultOutDeviceIndex);
    if(!mSoundIoOutDevice)
    {
        fprintf(stderr, "Could not acquire output device!!\n");
        return;
    }

    fprintf(stdout, "Output device name: %s\n", mSoundIoOutDevice->name);

    mSoundIoOutStream = soundio_outstream_create(mSoundIoOutDevice);
    mSoundIoOutStream->format = SoundIoFormatFloat32NE;
    mSoundIoOutStream->write_callback = writeCallbackSoundIO;
    mSoundIoOutStream->userdata = (void*) this;
    mSoundIoOutStream->software_latency = 0.01; // TODO: implement more flexible latency control

    if((err = soundio_outstream_open(mSoundIoOutStream)))
    {
        fprintf(stderr, "Unable to open outstream: %s!!\n", soundio_strerror(err));
    }

    mSampleRate = mSoundIoOutStream->sample_rate;
    fprintf(stdout, "Sample rate: %f\n", mSampleRate);

    if((err = soundio_outstream_start(mSoundIoOutStream)))
    {
        fprintf(stderr, "Unable to start outstream: %s!!\n", soundio_strerror(err));
        return;
    }

    mSoundIoEventLoopThread = std::make_unique<std::thread>(&AudioEngine::threadFuncSoundIoEventLoop, this);
}

void AudioEngine::generateSamples(StBuffers& outBuffers, size_t numSamples)
{
    // make a local copy of the chain vector
    mChainMutex.lock();
    auto chains = mChains;
    mChainMutex.unlock();

    size_t numChannels = outBuffers.buffers.size();

    // zero out all output buffers
    for(size_t j=0; j<numChannels; j++)
    {
        auto buffer = outBuffers.buffers[j];
        for(size_t i=0; i<numSamples; i++)
            buffer[i] = 0.0;
    }

    // create temporary sound buffers
    std::vector<StSample> tempBufferData(numChannels * numSamples);
    StBuffers tempBuffers;
    tempBuffers.buffers.resize(numChannels);
    tempBuffers.channelTypes = outBuffers.channelTypes;
    for(size_t i=0; i<numChannels; i++)
        tempBuffers.buffers[i] = tempBufferData.data() + i*numSamples;

    for(auto chain : chains)
    {
        // zero all temp buffers
        for(size_t i=0; i<numSamples*numChannels; i++)
            tempBufferData[i] = 0.0;

        chain->generateSamples(tempBuffers, mSampleRate, numSamples, nullptr);

        for(size_t i=0; i<numChannels; i++)
        {
            auto outbuf = outBuffers.buffers[i];
            auto tempbuf = tempBuffers.buffers[i];

            for(size_t j=0; j<numSamples; j++)
                outbuf[j] += tempbuf[j];
        }
    }

    if(mIsPlaybackOn)
        mCurrentTime += numSamples / mSampleRate;
}

void AudioEngine::addChain(std::shared_ptr<AudioModuleChain> chain)
{
    std::lock_guard<std::mutex> lock(mChainMutex);

    // make sure this module hasn't already been added
    for(size_t i=0; i<mChains.size(); i++)
        if(mChains[i].get() == chain.get())
            return;

    mChains.push_back(chain);
}

void AudioEngine::removeChain(AudioModuleChain* chain)
{
    std::lock_guard<std::mutex> lock(mChainMutex);

    for(size_t i=0; i<mChains.size(); i++)
        if(mChains[i].get() == chain)
        {
            mChains.erase(std::next(mChains.begin(), i));
            i--;
        }
}

void AudioEngine::seek(double time)
{
    std::lock_guard<std::mutex> lock(mChainMutex);

    mCurrentTime = time;
    for(auto& chain : mChains)
        chain->seek(time);
}

void AudioEngine::setPlaybackOn(bool playback)
{
    std::lock_guard<std::mutex> lock(mChainMutex);

    mIsPlaybackOn = playback;
    for(auto& chain : mChains)
        chain->setPlaybackOn(playback);
}

void AudioEngine::writeCallbackSoundIO(SoundIoOutStream* outstream, int frameCountMin, int frameCountMax)
{
    AudioEngine* engine = (AudioEngine*) outstream->userdata;
    engine->writeSoundIoSamples(outstream, frameCountMin, frameCountMax);
}

void AudioEngine::threadFuncSoundIoEventLoop()
{
    fprintf(stdout, "Waiting for SoundIO events!!\n");
    while(mShouldContinueSoundIoEventLoop)
    {
        soundio_wait_events(mSoundIo);
        fprintf(stdout, "SoundIO event received!!\n");
    }
}

void AudioEngine::writeSoundIoSamples(SoundIoOutStream* outstream, int frameCountMin, int frameCountMax)
{
    // ignore outstream for now; there should only be "mSoundIoOutStream"

    auto sampleConversionFunc = getSampleConvertFunc(outstream->format);
    if(sampleConversionFunc == nullptr)
    {
        fprintf(stderr, "No conversion function available for given sample format!!\n");
        return;
    }

    SoundIoChannelLayout& channelLayout = outstream->layout;
    size_t numChannels = channelLayout.channel_count;

    // create buffers
    std::vector<StSample> bufferData(numChannels * frameCountMax);
    StBuffers buffers;
    buffers.buffers.resize(numChannels);
    for(size_t i=0; i<numChannels; i++)
        buffers.buffers[i] = bufferData.data() + i * frameCountMax;
    buffers.channelTypes.resize(numChannels);
    buffers.channelTypes[0] = ST_CHANNEL_LEFT;
    buffers.channelTypes[1] = ST_CHANNEL_RIGHT;
    for(size_t i=2; i<numChannels; i++)
        buffers.channelTypes[i] = ST_CHANNEL_OTHER;

    // generate all samples
    this->generateSamples(buffers, frameCountMax);

    // write samples to outstream in a loop
    int framesLeft = frameCountMax;
    int frameIndex = 0;
    int err;
    SoundIoChannelArea* channelAreas;
    
    while(framesLeft > 0)
    {
        int frameCount = framesLeft;

        // begin write
        if((err = soundio_outstream_begin_write(mSoundIoOutStream, &channelAreas, &frameCount)))
        {
            fprintf(stderr, "SoundIO write error: %s!!\n", soundio_strerror(err));
            return;
        }

        if(frameCount == 0)
            break;

        // write to all channels
        for(size_t i=0; i<numChannels; i++)
            sampleConversionFunc(channelAreas[i].ptr, channelAreas[i].step, buffers.buffers[i] + frameIndex, frameCount);

        // end write
        if((err = soundio_outstream_end_write(mSoundIoOutStream)))
        {
            fprintf(stderr, "SoundIO write error: %s!!\n", soundio_strerror(err));
            return;
        }

        framesLeft -= frameCount;
        frameIndex += frameCount;
    }
}

void AudioEngine::stopSoundIo()
{
    // tell the SoundIO event loop thread that it should stop
    mShouldContinueSoundIoEventLoop = false;

    // destroying everything will send an event to the event loop thread
    if(mSoundIoOutStream != nullptr)
        soundio_outstream_destroy(mSoundIoOutStream);

    if(mSoundIoOutDevice != nullptr)
        soundio_device_unref(mSoundIoOutDevice);

    if(mSoundIo != nullptr)
        soundio_destroy(mSoundIo);

    mSoundIoEventLoopThread->join();
    mSoundIoEventLoopThread.reset();
}
