/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUDIO_ENGINE_H
#define AUDIO_ENGINE_H

#include <cstddef>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <memory>
#include <audiocommon/AudioCommon.h>
#include <audiocommon/MIDI.h>

struct SoundIo;
struct SoundIoOutStream;
struct SoundIoDevice;

class AudioModuleChain;

/**
    AudioEngine is responsible for interacting with audio hardware and
    coordinating the generation and mixing of sound samples. All active
    signal chains are registered with an instance of AudioEngine, which
    calls on them to generate samples when needed. There should never
    be a need to have more than one instance of AudioEngine.
*/
class AudioEngine
{
public:
    // PUBLIC ENUM TYPES
    enum IOMode
    {
        IO_MODE_NONE,
        IO_MODE_LIBSOUNDIO,
        IO_MODE_RENDER,
        IO_MODE_MAX_ENUM
    };

    AudioEngine();
    ~AudioEngine();
    AudioEngine(const AudioEngine& other) = delete;
    AudioEngine(AudioEngine&& other) = delete;
    AudioEngine& operator=(const AudioEngine& other) = delete;

    double getSampleRate();
    IOMode getIOMode();
    void setSampleRate(double sampleRate);
    void initSoundIO();
    void generateSamples(StBuffers& outBuffers, size_t numSamples);
    void addChain(std::shared_ptr<AudioModuleChain> module);
    void removeChain(AudioModuleChain* module);
    void seek(double time);
    void setPlaybackOn(bool isPlaybackOn);

private:
    void writeSoundIoSamples(SoundIoOutStream* outstream, int frameCountMin, int frameCountMax);
    static void writeCallbackSoundIO(SoundIoOutStream* outstream, int frameCountMin, int frameCountMax);
    void threadFuncSoundIoEventLoop();
    void stopSoundIo();

    SoundIo* mSoundIo = nullptr;
    SoundIoDevice* mSoundIoOutDevice = nullptr;
    SoundIoOutStream* mSoundIoOutStream = nullptr;
    double mSampleRate = 48000;
    double mCurrentTime = 0.0;
    bool mIsPlaybackOn = false;
    IOMode mIOMode = IO_MODE_NONE;
    std::vector<std::shared_ptr<AudioModuleChain>> mChains;
    std::mutex mChainMutex;
    std::atomic_bool mShouldContinueSoundIoEventLoop = true;
    std::unique_ptr<std::thread> mSoundIoEventLoopThread;
};

#endif // AUDIO_ENGINE_H