/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SampleConversion.h"

#include <cstdint>

void writeSamples_s16ne(char* dest, size_t step, double* src, size_t numSamples)
{
    constexpr double conversionFactor = ((double)INT16_MAX - (double)INT16_MIN) / 2.0;
    
    for(size_t i = 0; i < numSamples; i++)
    {
        int16_t* ptr = (int16_t*)(dest + step*i);
        *ptr = src[i] * conversionFactor;
    }
}

void writeSamples_s32ne(char* dest, size_t step, double* src, size_t numSamples)
{
    constexpr double conversionFactor = ((double)INT32_MAX - (double)INT32_MIN) / 2.0;
    
    for(size_t i = 0; i < numSamples; i++)
    {
        int32_t* ptr = (int32_t*)(dest + step*i);
        *ptr = src[i] * conversionFactor;
    }
}

void writeSamples_float32ne(char* dest, size_t step, double* src, size_t numSamples)
{
    for(size_t i=0; i<numSamples; i++)
    {
        float* ptr = (float*)(dest + step*i);
        *ptr = src[i];
    }
}

void writeSamples_float64ne(char* dest, size_t step, double* src, size_t numSamples)
{
    for(size_t i=0; i<numSamples; i++)
    {
        double* ptr = (double*)(dest + step*i);
        *ptr = src[i];
    }
}
