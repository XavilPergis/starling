/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SAMPLE_CONVERSION_H
#define SAMPLE_CONVERSION_H

#include <cstddef>

typedef void (*writeSamplesPFN)(char* dest, size_t step, double* src, size_t numSamples);
void writeSamples_s16ne(char* dest, size_t step, double* src, size_t numSamples);
void writeSamples_s32ne(char* dest, size_t step, double* src, size_t numSamples);
void writeSamples_float32ne(char* dest, size_t step, double* src, size_t numSamples);
void writeSamples_float64ne(char* dest, size_t step, double* src, size_t numSamples);

#endif // SAMPLE_CONVERSION_H