/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ChainContainerWidget.h"
#include <QEvent>
#include <QPainter>
#include <QBoxLayout>
#include <qboxlayout.h>
#include "ModuleContainerWidget.h"
#include <audioengine/AudioModuleChain.h>
#include <QPushButton>
#include <QDragEnterEvent>
#include <QMimeData>
#include "ModuleRegistry.h"

ChainContainerWidget::ChainContainerWidget(QWidget* parent) : QFrame(parent)
{
    QHBoxLayout* layout = new QHBoxLayout;
    this->setLayout(layout);
    QSpacerItem* hSpacer = new QSpacerItem(452, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    layout->addItem(hSpacer);

    this->setAcceptDrops(true);
}

void ChainContainerWidget::setModuleChain(AudioModuleChain* chain)
{
    mChain = chain;

    QHBoxLayout* layout = qobject_cast<QHBoxLayout*>(this->layout());
    layout->setContentsMargins(0, 0, 0, 0);
    for(auto& module : chain->getModules())
    {
        ModuleContainerWidget* moduleContainer = new ModuleContainerWidget(this, module.get());
        layout->insertWidget(layout->count()-1, moduleContainer);
    }
}

void ChainContainerWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("application/starling.module.list"))
        event->accept();
}

void ChainContainerWidget::dropEvent(QDropEvent* event)
{
    if(event->mimeData()->hasFormat("application/starling.module.list"))
    {
        QByteArray data = event->mimeData()->data("application/starling.module.list");
        QDataStream stream(&data, QIODevice::ReadOnly);

        QString internalName;
        stream >> internalName;

        auto module = ModuleRegistry::instance()->makeModule(internalName);

        if(module.get() != nullptr)
        {
            auto modules = mChain->getModules();
            modules.push_back(module);
            mChain->updateModules(modules);

            ModuleContainerWidget* moduleContainer = new ModuleContainerWidget(this, module.get());
            QHBoxLayout* layout = qobject_cast<QHBoxLayout*>(this->layout());
            layout->insertWidget(layout->count()-1, moduleContainer);
        }
    }
}
