/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ModuleRegistry.h"
#include <QString>
#include <memory>
#include <qabstractitemmodel.h>
#include <qnamespace.h>
#include <QMimeData>
#include <QDataStream>
#include "sine/SineModule.h"
#include "distort/DistortionModule.h"

ModuleRegistry* mInstance = nullptr;

ModuleRegistry::ModuleRegistry(QObject* parent) : QAbstractItemModel(parent)
{
    mInstance = this;

    mRootItem.isDirectory = true;
    this->populate();
}

ModuleRegistry* ModuleRegistry::instance()
{
    return mInstance;
}

std::shared_ptr<AudioModule> ModuleRegistry::makeModule(QString internalName)
{
    auto maker = mModuleMap.find(internalName);

    if(maker != mModuleMap.end())
        return maker->second->moduleMaker->makeModule();

    return std::shared_ptr<AudioModule>();
}

int ModuleRegistry::rowCount(const QModelIndex& parent) const
{
    if(parent.column() > 0)
        return 0;

    const Item* parentItem;
    if(parent.isValid())
        parentItem = static_cast<Item*>(parent.internalPointer());
    else
        parentItem = &mRootItem;

    return parentItem->children.size();
}

int ModuleRegistry::columnCount(const QModelIndex& parent) const
{
    return 1;
}

QVariant ModuleRegistry::data(const QModelIndex& index, int role) const
{
    // no data for the root
    if(!index.isValid() || index.internalPointer() == &mRootItem)
        return QVariant();
    
    Item* item = static_cast<Item*>(index.internalPointer());

    switch(role)
    {
    case Qt::DisplayRole:
        if(item->isDirectory)
            return item->name;
        else
            return item->moduleMaker->getDisplayName();
    default:
        return QVariant();
    }
}

Qt::ItemFlags ModuleRegistry::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::NoItemFlags;

    Item* item = static_cast<Item*>(index.internalPointer());

    if(item == &mRootItem || item == nullptr)
        return QAbstractItemModel::flags(index);

    if(item->isDirectory)
        return QAbstractItemModel::flags(index);

    return Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
}

QVariant ModuleRegistry::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
        return "Instruments & Effects";
    
    return QVariant();
}

QModelIndex ModuleRegistry::index(int row, int column, const QModelIndex& parent) const
{
    if(column != 0)
        return QModelIndex();

    const Item* parentItem;
    if(parent.isValid())
        parentItem = static_cast<Item*>(parent.internalPointer());
    else
        parentItem = &mRootItem;

    if(row >= parentItem->children.size())
        return QModelIndex();

    return createIndex(row, 0, parentItem->children[row].get());
}

QModelIndex ModuleRegistry::parent(const QModelIndex& index) const
{
    if(!index.isValid())
        return QModelIndex();

    if(index.internalPointer() == &mRootItem || index.internalPointer() == nullptr)
        return QModelIndex();

    Item* parent = static_cast<Item*>(index.internalPointer())->parent;

    if(parent == &mRootItem)
        return createIndex(0, 0, parent);

    Item* grandparent = parent->parent;

    // search for parent
    for(size_t i=0; i<grandparent->children.size(); i++)
        if(grandparent->children[i].get() == parent)
            return createIndex(i, 0, parent);

    return QModelIndex();
}

QStringList ModuleRegistry::mimeTypes() const
{
    QStringList types;
    types << "application/starling.module.list";
    return types;
}

QMimeData* ModuleRegistry::mimeData(const QModelIndexList& indices) const
{
    QMimeData* mimeData = new QMimeData;
    QByteArray encodedData;
    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    for(const QModelIndex& index : indices)
    {
        if(index.isValid())
        {
            Item* item = static_cast<Item*>(index.internalPointer());

            if(!item->isDirectory)
                stream << item->moduleMaker->getInternalName();
        }
    }

    mimeData->setData("application/starling.module.list", encodedData);
    return mimeData;
}

void ModuleRegistry::populate()
{
    mRootItem.children.clear();
    mModuleMap.clear();

    // built-in modules with unique classes
    addModuleMaker(std::make_unique<TemplateModuleMaker<SineModule>>("Sine Synth", "sinesynth"));
    addModuleMaker(std::make_unique<TemplateModuleMaker<DistortionModule>>("Distortion", "distortion"));
}

void ModuleRegistry::addModuleMaker(std::unique_ptr<ModuleMaker>&& moduleMaker, Item* parent)
{
    if(parent == nullptr)
        parent = &mRootItem;

    size_t index = parent->children.size();
    parent->children.push_back(std::make_unique<Item>());

    auto* item = parent->children[index].get();

    item->isDirectory = false;
    item->moduleMaker.swap(moduleMaker);
    item->parent = parent;

    mModuleMap.insert(std::pair{item->moduleMaker->getInternalName(), item});
}

void ModuleRegistry::addDirectory(QString name, Item* parent)
{
    if(parent == nullptr)
        parent = &mRootItem;

    size_t index = parent->children.size();
    parent->children.push_back(std::make_unique<Item>());

    parent->children[index]->isDirectory = true;
    parent->children[index]->name = name;
    parent->children[index]->parent = parent;
}
