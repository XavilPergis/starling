/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DistortionModule.h"
#include <cmath>
#include <QFrame>
#include "ui_distort.h"

inline double distort(double invalue, double order, double postgain)
{
    double outvalue = invalue;
    double multiplier = (invalue > 0) ? 1.0 : -1.0;
    outvalue *= multiplier;
    if(outvalue > 1.0)
        outvalue = 1.0;
    outvalue = 1.0-pow(1.0-outvalue, order);
    return outvalue * multiplier * postgain;
}

void DistortionModule::generateSamples(StBuffers& inBuffers, StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI)
{
    double order = (double)mOrderParam / 32.0 + 1.0;
    double postgain = (double)mPostGainParam / 127.0;

    for(size_t i=0; i<inBuffers.buffers.size(); i++)
    {
        auto inbuf = inBuffers.buffers[i];
        auto outbuf = outBuffers.buffers[i];

        for(size_t j=0; j<numSamples; j++)
            outbuf[j] = distort(inbuf[j], order, postgain);
    }
}

void DistortionModule::reset()
{
    
}

QWidget* DistortionModule::makeGUI(QWidget* parent)
{
    QFrame* frame = new QFrame(parent);
    Ui::DistortGUIFrame ui;
    ui.setupUi(frame);

    // set initial values
    ui.orderDial->setValue(mOrderParam);
    ui.postGainDial->setValue(mPostGainParam);

    // connect
    connect(ui.orderDial, &QDial::valueChanged, this, &DistortionModule::setOrder);
    connect(ui.postGainDial, &QDial::valueChanged, this, &DistortionModule::setPostGain);

    return frame;
}

void DistortionModule::setOrder(int value)
{
    mOrderParam = value;
}

void DistortionModule::setPostGain(int value)
{
    mPostGainParam = value;
}
