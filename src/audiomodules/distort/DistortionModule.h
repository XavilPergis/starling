/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DISTORTION_MODULE_H
#define DISTORTION_MODULE_H

#include <QObject>
#include <audiocommon/AudioModule.h>

/*
    DistortionModule is a simple distortion effect which uses exponential
    clipping. Its functionality will likely be expanded in the future.
 */
class DistortionModule : public AudioModule
{
    Q_OBJECT

public:
    virtual void generateSamples(StBuffers& inBuffers, StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI) override;
    virtual void reset() override;
    QWidget* makeGUI(QWidget* parent) override;

public slots:
    void setOrder(int value);
    void setPostGain(int value);

private:
    int mOrderParam = 64;
    int mPostGainParam = 100;
};

#endif // DISTORTION_MODULE_H