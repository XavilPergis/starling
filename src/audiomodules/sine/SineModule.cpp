/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SineModule.h"
#include <cmath>
#include <QWidget>
#include "ui_sine.h"

constexpr double L_PI = 3.141592653589793115997963468544185161590576171875;

double SineModule::adsr(double time)
{
    if(time < mAttack)
        return (time/mAttack);
    else if(time < mAttack+mDecay)
        return (1.0 - (time-mAttack)/mDecay)*(1.0-mSustain) + mSustain;
    else if(time < mAttack+mDecay+mRelease)
        return (1.0 - (time-mAttack-mDecay)/mRelease)*mSustain;
    else
        return 0.0;
}

void SineModule::generateSamples(StBuffers& inBuffers, StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI)
{
    MIDIParser midi(*inMIDI);

    StSample *outleft, *outright;
    outleft = outBuffers.buffers[0];
    outright = (outBuffers.buffers.size() > 1) ? outBuffers.buffers[1] : outBuffers.buffers[0];

    double deltaPhase = pow(2.0, ((double)mNote - 69.0) / 12.0) * 440.0 * L_PI / sampleRate;
    double deltaTime = 1.0/sampleRate;

    for(size_t i=0; i<numSamples; i++)
    {
        while(midi.next(i))
        {
            if(midi.isNoteOn())
            {
                mTime = adsr(mTime)*mAttack;
                mNote = midi.note();
                deltaPhase = pow(2.0, ((double)mNote - 69.0) / 12.0) * 440.0 * L_PI / sampleRate;
                isNoteOn = true;
            }
            else if(midi.isNoteOff())
            {
                if(midi.note() == mNote)
                    isNoteOn = false;
            }
        }
        double value = sin(mPhase) * adsr(mTime)/2.0;
        outleft[i] = value;
        outright[i] = value;

        mPhase += deltaPhase;
        mTime += deltaTime;
        if(isNoteOn && mTime > mAttack + mDecay)
            mTime = mAttack + mDecay;
    }
}

void SineModule::reset()
{
    mTime = 1000.0;
}

QWidget* SineModule::makeGUI(QWidget* parent)
{
    QFrame* frame = new QFrame(parent);
    Ui::SineGUIFrame ui;
    ui.setupUi(frame);

    // set initial values
    ui.attackDial->setValue(pow(mAttack, 1.0/3.0)*127);
    ui.decayDial->setValue(mDecay * 127);
    ui.sustainDial->setValue(mSustain * 127);
    ui.releaseDial->setValue(mRelease * 127);

    // connect
    connect(ui.attackDial, &QDial::valueChanged, this, &SineModule::setAttack);
    connect(ui.decayDial, &QDial::valueChanged, this, &SineModule::setDecay);
    connect(ui.sustainDial, &QDial::valueChanged, this, &SineModule::setSustain);
    connect(ui.releaseDial, &QDial::valueChanged, this, &SineModule::setRelease);

    return frame;
}

void SineModule::setAttack(int value)
{
    mAttack = pow((double)value / 127.0, 3.0);
}

void SineModule::setDecay(int value)
{
    mDecay = (double)value / 127.0;
}

void SineModule::setSustain(int value)
{
    mSustain = (double)value / 127.0;
}

void SineModule::setRelease(int value)
{
    mRelease = (double)value / 127.0;
}
