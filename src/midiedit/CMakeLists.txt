add_library(st_midiedit
    PianoRollWidget.cpp
    PianoRollWidget.h
    NoteItem.cpp
    NoteItem.h
)
target_link_libraries(st_midiedit PRIVATE Qt5::Widgets st_audiocommon st_guicommon)