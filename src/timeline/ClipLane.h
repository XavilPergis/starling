/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLIP_LANE_H
#define CLIP_LANE_H

#include <mutex>
#include <vector>
#include <memory>
#include <audioengine/AudioModuleChain.h>
#include <audiocommon/MIDIClip.h>

class LaneWidget;

/**
    ClipLane is a subclass of AudioModuleChain, which owns clips containing
    data that can be fed into the inputs of the signal chain. It overrides
    generateSamples() so that it can read this data during playback.
 */
class ClipLane : public AudioModuleChain
{
public:
    ClipLane(LaneWidget* widget);
    virtual void generateSamples(StBuffers& outBuffers, double sampleRate, size_t numSamples, MIDISequence* inMIDI) override;
    void seek(double time) override;
    void setPlaybackOn(bool playback) override;
    std::shared_ptr<MIDIClip> makeClip();
    void removeClip(MIDIClip* clip);

private:
    std::vector<std::shared_ptr<MIDIClip>> mClips;
    std::shared_mutex mMutex;
    LaneWidget* mWidget = nullptr;
    double mCurrentTime = 0.0;
    bool mIsPlaybackOn = false;
};

#endif // CLIP_LANE_H