/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ClipWidget.h"
#include <QEvent>
#include <QPolygon>
#include <QPainter>
#include <QMouseEvent>
#include <qevent.h>
#include <qnamespace.h>
#include "LaneWidget.h"

ClipWidget::ClipWidget(LaneWidget* parent) : QWidget(parent)
{
    this->setMinimumWidth(10);
    this->setMinimumHeight(10);
    this->setMouseTracking(true);

    mClip = parent->getClipLane()->makeClip();
}

void ClipWidget::updateTimeScale(double pixelsPerBeat)
{
    double start = mClip->startTime();
    double end = mClip->endTime();
    QRect geom = this->geometry();
    geom.setX(start * pixelsPerBeat);
    geom.setWidth((end - start) * pixelsPerBeat);
}

MIDIClip* ClipWidget::getClip()
{
    return mClip.get();
}

void ClipWidget::mouseMoveEvent(QMouseEvent* event)
{
    switch(mCurrentDragAction)
    {
    case DRAG_NONE:
        if(mousePosIsAtLeft(event->pos()) || mousePosIsAtRight(event->pos()))
            this->setCursor(Qt::SizeHorCursor);
        else
            this->unsetCursor();
        break;

    case DRAG_RESIZE_LEFT:
        {
            QRect geom = this->geometry();
            geom.setX(geom.x() + event->pos().x());
            this->setGeometry(geom);
            updateStartEndTime();
        }
        break;

    case DRAG_RESIZE_RIGHT:
        {
            QRect geom = this->geometry();
            geom.setWidth(event->pos().x());
            this->setGeometry(geom);
            updateStartEndTime();
        }
        break;

    case DRAG_MOVE_WHOLE:
        {
            QRect geom = this->geometry();
            int tempWidth = geom.width();
            geom.setX(geom.x() + event->pos().x() - mDragInitialMouseX);
            geom.setWidth(tempWidth);
            this->setGeometry(geom);
            updateStartEndTime();
        }
        break;

    case DRAG_MAX_ENUM:
        // This should never happen. If it somehow does happen, stop dragging to prevent issues.
        mCurrentDragAction = DRAG_NONE;
        break;
    }
}

void ClipWidget::mousePressEvent(QMouseEvent* event)
{
    // Check if we should start dragging one of the size
    if(mousePosIsAtLeft(event->pos()))
        mCurrentDragAction = DRAG_RESIZE_LEFT;
    else if(mousePosIsAtRight(event->pos()))
        mCurrentDragAction = DRAG_RESIZE_RIGHT;
    else
    {
        mCurrentDragAction = DRAG_MOVE_WHOLE;
        this->setCursor(Qt::DragMoveCursor);
    }
    
    mDragInitialMouseX = event->pos().x();
}

void ClipWidget::mouseReleaseEvent(QMouseEvent* event)
{
    // Stop dragging
    if(mCurrentDragAction == DRAG_MOVE_WHOLE)
        this->unsetCursor();
    mCurrentDragAction = DRAG_NONE;
}

void ClipWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
    emit openPianoRoll(this);
}

void ClipWidget::enterEvent(QEvent* event)
{
    /**
    if(mousePosIsAtLeft(event->position()) || mousePosIsAtRight(event->position()))
        this->setCursor(Qt::SizeHorCursor);
    else
        this->unsetCursor();    //*/
}

void ClipWidget::leaveEvent(QEvent* event)
{
    this->unsetCursor();
}

void ClipWidget::paintEvent(QPaintEvent* event)
{
    // render shape
    QPolygon poly;
    poly.putPoints(0, 5, 
        0, 0,
        this->width() - 5, 0,
        this->width(), 5,
        this->width(), this->height(),
        0, this->height()   );
    
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::red);
    painter.drawConvexPolygon(poly);

    // render notes
    // TODO: make this prettier
    auto notes = mClip->getNotes();
    if(notes->size() > 0)
    {
        double pixelsPerBeat = ((LaneWidget*)this->parent())->pixelsPerBeat();
        uint8_t lowestNoteValue = 255;
        uint8_t highestNoteValue = 0;

        // find lowest and highest value
        for(auto& note : *notes)
        {
            uint8_t value = note.second.note;
            if(value > highestNoteValue)
                highestNoteValue = value;
            if(value < lowestNoteValue)
                lowestNoteValue = value;
        }

        double valueHeight = (double)this->height() / (double)(highestNoteValue - lowestNoteValue + 1);
        painter.setRenderHint(QPainter::Antialiasing, false);

        // draw each note as a rectangle
        for(auto& note : *notes)
        {
            int left = note.second.onTime * pixelsPerBeat;
            int width = (note.second.offTime * pixelsPerBeat) - left;
            int top = (highestNoteValue - note.second.note) * valueHeight;
            int height = (highestNoteValue - note.second.note + 1) * valueHeight - top;
            painter.fillRect(left, top, width, height, Qt::black);
        }
    }
}

bool ClipWidget::mousePosIsAtLeft(const QPointF& position)
{
    return (position.x() < BORDER_GRAB_SIZE);
}

bool ClipWidget::mousePosIsAtRight(const QPointF& position)
{
    return (position.x() > this->width() - BORDER_GRAB_SIZE);
}

void ClipWidget::updateStartEndTime()
{
    auto& geom = this->geometry();
    double pixelsPerBeat = ((LaneWidget*)this->parent())->pixelsPerBeat();
    mClip->setStartEndTime((double)geom.x()/pixelsPerBeat, (double)(geom.x()+geom.width())/pixelsPerBeat);
}
