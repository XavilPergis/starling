/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLIP_WIDGET_H
#define CLIP_WIDGET_H

#include <memory>
#include <QWidget>
#include <QEvent>
#include <audiocommon/MIDIClip.h>

class LaneWidget;

/**
    ClipWidget is a custom widget which provides an interface for moving
    and resizing a particular clip within a ClipLane. Its parent is always
    a LaneWidget. It also provides an interface for opening the proper
    editor for the underlying data; for MIDI clips, that is the piano roll.
 */
class ClipWidget : public QWidget
{
    Q_OBJECT

public:
    ClipWidget(LaneWidget* parent = nullptr);
    void updateTimeScale(double pixelsPerBeat);
    MIDIClip* getClip();

signals:
    void openPianoRoll(ClipWidget* clipWidget);

protected:
    // Qt events
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void enterEvent(QEvent* event) override;
    void leaveEvent(QEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

private:
    bool mousePosIsAtLeft(const QPointF& position);
    bool mousePosIsAtRight(const QPointF& position);
    void updateStartEndTime();

    const int BORDER_GRAB_SIZE = 6;

    enum DragAction     // not to be confused with QT's drag-and-drop system
    {                   // these are the actions that can be happening while the mouse is held and dragged
        DRAG_NONE,
        DRAG_RESIZE_LEFT,
        DRAG_RESIZE_RIGHT,
        DRAG_MOVE_WHOLE,
        DRAG_MAX_ENUM
    };

    std::shared_ptr<MIDIClip> mClip;
    ClipWidget::DragAction mCurrentDragAction = DRAG_NONE;
    qreal mDragInitialMouseX = 0;
};

#endif  // CLIP_WIDGET_H