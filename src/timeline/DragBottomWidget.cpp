/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DragBottomWidget.h"
#include "LaneWidget.h"
#include <QMouseEvent>
#include <qnamespace.h>
#include <QPaintEvent>
#include <QPainter>

DragBottomWidget::DragBottomWidget(LaneWidget* parent) : QWidget(parent)
{
    this->setCursor(Qt::SizeVerCursor);
    this->setMouseTracking(true);
}

void DragBottomWidget::mouseMoveEvent(QMouseEvent* event)
{
    if(this->mIsDraggingBottom)
    {
        // Resize widget vertically
        qreal newHeight = mapToParent(event->pos()).y();
        if(newHeight < MIN_LANE_HEIGHT)
            newHeight = MIN_LANE_HEIGHT;
        this->parentWidget()->setFixedHeight(newHeight);
    }
}

void DragBottomWidget::mousePressEvent(QMouseEvent* event)
{
    
    this->mIsDraggingBottom = true;
}

void DragBottomWidget::mouseReleaseEvent(QMouseEvent* event)
{
    this->mIsDraggingBottom = false;
}

void DragBottomWidget::enterEvent(QEvent* event)
{
    
}

void DragBottomWidget::leaveEvent(QEvent* event)
{

}

void DragBottomWidget::paintEvent(QPaintEvent* event)
{
    // Do nothing. This widget is invisible.
}