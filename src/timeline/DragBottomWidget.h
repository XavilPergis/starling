/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRAG_BOTTOM_WIDGET_H
#define DRAG_BOTTOM_WIDGET_H

#include <QWidget>

class LaneWidget;

/**
    DragBottomWidget is a custom widget which assists in resizing another
    widget vertically, by holding the mouse button and dragging at the bottom.
    It is meant to be kept in front of all of its parent's children, and located
    at the bottom. It is used by LaneWidget.
 */
class DragBottomWidget : public QWidget
{
    Q_OBJECT

public:
    DragBottomWidget(LaneWidget* parent);

private:
    // MOUSE EVENTS
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void enterEvent(QEvent* event) override;
    void leaveEvent(QEvent* event) override;

    // PAINT EVENT
    void paintEvent(QPaintEvent* event) override;

    // PRIVATE CONSTANTS
    const qreal MIN_LANE_HEIGHT = 20;

    // PRIVATE VARIABLES
    bool mIsDraggingBottom = false; // True when the user is resizing this widget
    qreal dragHeight = 0;
};

#endif // DRAG_BOTTOM_WIDGET_H