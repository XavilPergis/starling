/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LaneHeaderWidget.h"
#include "LaneWidget.h"
#include <QPaintEvent>
#include <QPainter>

LaneHeaderWidget::LaneHeaderWidget(QWidget* parent, LaneWidget* laneWidget) : QWidget(parent)
{
    mLaneWidget = laneWidget;
    mLaneWidget->setLaneHeaderWidget(this);
}

void LaneHeaderWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    QRect rect(0, 0, width(), height());
    painter.fillRect(rect, Qt::red);
}

void LaneHeaderWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
    emit openSignalChainEditor(mLaneWidget);
}
