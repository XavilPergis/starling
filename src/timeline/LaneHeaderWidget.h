/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LANE_HEADER_WIDGET_H
#define LANE_HEADER_WIDGET_H

#include <QWidget>

class LaneWidget;

/**
    LaneHeaderWidget is a custom widget which provides an interface for
    adjusting parameters of a given audio track. Each LaneHeaderWidget
    has a corresponding LaneWidget, which it is kept next to horizontally.
 */
class LaneHeaderWidget : public QWidget
{
    Q_OBJECT

public:
    LaneHeaderWidget(QWidget* parent, LaneWidget* laneWidget);

signals:
    void openSignalChainEditor(LaneWidget* laneWidget);

protected:
    void paintEvent(QPaintEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;

private:
    LaneWidget* mLaneWidget = nullptr;
};

#endif // LANE_HEADER_WIDGET_H