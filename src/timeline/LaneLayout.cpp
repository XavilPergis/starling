/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LaneLayout.h"

void LaneLayout::addItem(QLayoutItem* item)
{
    mLayoutItems.push_back(std::unique_ptr<QLayoutItem>(item));
}

QSize LaneLayout::sizeHint() const
{
    // This is not used; size is determined by the lane's minimum height,
    // and the available horizontal space.
    return QSize(1, 1);
}

Qt::Orientations LaneLayout::expandingDirections() const
{
    return Qt::Horizontal;
}

void LaneLayout::setGeometry(const QRect& rect)
{
    QLayout::setGeometry(rect);
    doLayout(rect);
}

QLayoutItem* LaneLayout::itemAt(int index) const
{
    if(index < mLayoutItems.size())
        return mLayoutItems[index].get();
    else
        return nullptr;
}

QLayoutItem* LaneLayout::takeAt(int index)
{
    if(index < mLayoutItems.size())
    {
        // release ownership, erase item from vector, return pointer
        auto* ptr = mLayoutItems[index].release();
        mLayoutItems.erase(std::next(mLayoutItems.begin(), index));
        return ptr;
    }
    else
        return nullptr;
}

int LaneLayout::count() const
{
    return mLayoutItems.size();
}

QSize LaneLayout::minimumSize() const
{
    return QSize(1, 1);
}

void LaneLayout::doLayout(const QRect& rect)
{
    for(auto& item : mLayoutItems)
    {
        // Only adjust the item vertically
        // TODO: prevent horizontal overlap

        QRect geom = item->geometry();
        geom.setTop(rect.top());
        geom.setBottom(rect.bottom());
        item->setGeometry(geom);
    }
}
