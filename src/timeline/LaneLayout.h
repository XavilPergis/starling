/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LANE_LAYOUT_H
#define LANE_LAYOUT_H

#include <vector>
#include <memory>
#include <QLayout>
#include <QLayoutItem>

/**
    LaneLayout is a custom layout used by LaneWidget to manage the positioning
    and sizing of individual ClipWidgets. Because the horizontal positions of
    these ClipWidgets represent the start and end times of the underlying data,
    this layout does not use the usual methods of arranging children to
    fit within the allotted space.
 */
class LaneLayout : public QLayout
{
    Q_OBJECT

public:
    // OVERRIDDEN QLayout FUNCTIONALITY
    void addItem(QLayoutItem* item) override;
    QSize sizeHint() const override;
    Qt::Orientations expandingDirections() const override;
    void setGeometry(const QRect& r) override;
    QLayoutItem* itemAt(int index) const override;
    QLayoutItem* takeAt(int index) override;
    int count() const override;
    QSize minimumSize() const override;

private:
    void doLayout(const QRect& rect);

    std::vector<std::unique_ptr<QLayoutItem>> mLayoutItems;
};

#endif // LANE_LAYOUT_H