/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LaneWidget.h"
#include "LaneLayout.h"
#include "DragBottomWidget.h"
#include "ClipWidget.h"
#include <QEvent>
#include <QPainter>
#include <QLayout>
#include <QMouseEvent>
#include <memory>
#include "LaneHeaderWidget.h"

LaneWidget::LaneWidget(QWidget* parent) : QWidget(parent)
{
    this->setMinimumHeight(MIN_LANE_HEIGHT);
    this->setMouseTracking(true);
    this->setLayout(new LaneLayout());

    mClipLane = std::make_shared<ClipLane>(this);

    mDragWidget = std::make_unique<DragBottomWidget>(this);
    mDragWidget->setFixedSize(this->width(), this->height() - BORDER_GRAB_SIZE);

    addClip();
}

void LaneWidget::raiseDragWidget()
{
    mDragWidget->raise();
}

std::shared_ptr<ClipLane> LaneWidget::getClipLane()
{
    return mClipLane;
}

double LaneWidget::pixelsPerBeat()
{
    return mPixelsPerBeat;
}

ClipWidget* LaneWidget::getClip()
{
    return mClips[0];
}

void LaneWidget::setPixelsPerBeat(double ppb)
{
    mPixelsPerBeat = ppb;
    // TODO: update ClipWidgets to reflect change in scale
}

void LaneWidget::moveEvent(QMoveEvent* event)
{
    // move the dragger widget into position

    mDragWidget->setFixedSize(this->width(), BORDER_GRAB_SIZE);
    mDragWidget->setGeometry(0, this->height() - BORDER_GRAB_SIZE, this->width(), BORDER_GRAB_SIZE);
}

void LaneWidget::resizeEvent(QResizeEvent* event)
{
    // move the dragger widget into position
    mDragWidget->setFixedSize(this->width(), BORDER_GRAB_SIZE);
    mDragWidget->setGeometry(0, this->height() - BORDER_GRAB_SIZE, this->width(), BORDER_GRAB_SIZE);

    if(mHeaderWidget != nullptr)
        mHeaderWidget->setFixedHeight(this->height());
}

void LaneWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);

    QPen pen;
    pen.setColor(palette().color(QPalette::Shadow));
    pen.setWidth(2);

    QRect rect(0, 0, this->width(), this->height());
    painter.setPen(pen);
    painter.drawRect(rect);

    pen.setWidth(1);
    painter.setPen(pen);
    for(double x=mPixelsPerBeat; x<width(); x+=mPixelsPerBeat)
        painter.drawLine(x, 0, x, height());
}

ClipWidget* LaneWidget::addClip()
{
    ClipWidget* clipWidget = new ClipWidget(this);
    this->layout()->addWidget(clipWidget);
    mClips.push_back(clipWidget);

    connect(clipWidget, &ClipWidget::openPianoRoll, this, &LaneWidget::openPianoRoll);

    mDragWidget->raise();

    return clipWidget;
}

void LaneWidget::setLaneHeaderWidget(LaneHeaderWidget* headerWidget)
{
    mHeaderWidget = headerWidget;
    mHeaderWidget->setFixedHeight(this->height());
}

bool LaneWidget::mousePosIsAtBottom(const QPointF& position)
{
    return (position.y() > this->height() - BORDER_GRAB_SIZE);
}
