/*
 * This file is part of the Starling DAW
 *
 * SPDX-FileCopyrightText: 2021 F. R. Duggan <ottercode@yahoo.com>
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LANE_WIDGET_H
#define LANE_WIDGET_H

#include <QWidget>
#include <QEvent>
#include <QWidget>
#include <memory>
#include <vector>
#include <qevent.h>
#include "ClipLane.h"
#include "DragBottomWidget.h"

class ClipWidget;
class LaneHeaderWidget;

/**
    LaneWidget is a custom widget which provides an interface for creating and
    arranging clips within a track. It shares ownership of an underlying
    ClipLane with the AudioEngine. Individual ClipWidgets represent each clip
    within the underlying ClipLane. Each LaneWidget has a corresponding
    LaneHeaderWidget, which provides an interface for adjusting its parameters.
 */
class LaneWidget : public QWidget
{
    Q_OBJECT

public:
    LaneWidget(QWidget* parent);
    void raiseDragWidget();
    std::shared_ptr<ClipLane> getClipLane();
    double pixelsPerBeat();
    void setPixelsPerBeat(double ppb);
    ClipWidget* getClip();

signals:
    void openPianoRoll(ClipWidget* clipWidget);
    void openSignalChainEditor(LaneWidget* laneWidget);

private:
    friend class LaneHeaderWidget;

    // Qt events
    void moveEvent(QMoveEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

    // private member functions
    ClipWidget* addClip();
    void setLaneHeaderWidget(LaneHeaderWidget* headerWidget);
    bool mousePosIsAtBottom(const QPointF& position);

    const qreal MIN_LANE_HEIGHT = 20;
    const int BORDER_GRAB_SIZE = 6;

    std::vector<ClipWidget*> mClips;
    std::shared_ptr<ClipLane> mClipLane;
    std::unique_ptr<DragBottomWidget> mDragWidget;
    LaneHeaderWidget* mHeaderWidget = nullptr;
    double mPixelsPerBeat = 64.0;
};

#endif //LANE_WIDGET_H